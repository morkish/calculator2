package com.example.student.calculator2;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import static com.example.student.calculator2.R.id.add;
import static com.example.student.calculator2.R.id.n1EditText;
import static com.example.student.calculator2.R.id.n2EditText;
import static com.example.student.calculator2.R.id.score;

public class MainActivity extends AppCompatActivity {
EditText n1Edi, n2Edi, score1;
Button addButton, reduceButton, multiButton, divideButton;
    private ProgressBar progressBar;

    LogicService logicService;
    boolean mBound = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        n1Edi = (EditText) findViewById(R.id.n1EditText);
        n2Edi  = (EditText) findViewById(R.id.n2EditText);
        score1 = (EditText) findViewById(R.id.score);
        addButton = (Button) findViewById(R.id.addButton);
        reduceButton = (Button) findViewById(R.id.reduceButton);
        multiButton = (Button) findViewById(R.id.multiButton);
        divideButton = (Button) findViewById(R.id.divideButton);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBound){
                    Double n1 = Double.valueOf(n1Edi.getText().toString());
                    Double n2 = Double.valueOf(n2Edi.getText().toString());
                    score1.setText(String.valueOf(logicService.add(n1,n2)));
                }
            }
        });
        reduceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBound){
                    Double n1 = Double.valueOf(n1Edi.getText().toString());
                    Double n2 = Double.valueOf(n2Edi.getText().toString());
                    score1.setText(String.valueOf(logicService.reduce(n1,n2)));
                }
            }
        });
        multiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBound){
                    Double n1 = Double.valueOf(n1Edi.getText().toString());
                    Double n2 = Double.valueOf(n2Edi.getText().toString());
                    score1.setText(String.valueOf(logicService.multiply(n1,n2)));
                }
            }
        });
        divideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBound){
                    Double n1 = Double.valueOf(n1Edi.getText().toString());
                    Double n2 = Double.valueOf(n2Edi.getText().toString());
                    score1.setText(String.valueOf(logicService.divide(n1,n2)));
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private ServiceConnection logicConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            LogicService.LocalBinder binder = (LogicService.LocalBinder) service;
            logicService = binder.getService();
            mBound = true;
            Toast.makeText(MainActivity.this, "Logic Service Connected!",
                    Toast.LENGTH_SHORT).show();
        }
        public void onServiceDisconnected(ComponentName className) {
            logicService = null;
            mBound = false;
            Toast.makeText(MainActivity.this, "Logic Service Disconnected!",
                    Toast.LENGTH_SHORT).show();
        }
    };
    @Override
    protected void onStart() {
        super.onStart();
        if (!mBound) {
            this.bindService(new Intent(MainActivity.this,LogicService.class),
                    logicConnection, Context.BIND_AUTO_CREATE);
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            mBound = false;
            this.unbindService(logicConnection);
        }
    }
    public void resolvePi(View v) {
        progressBar.setProgress(0);
        new PiComputeTask().execute();
    }

}

//OSOBNY watek

public class PiComputeTask extends AsyncTask<Void, Integer, Double> {
    @Override
    protected Double doInBackground(Void... voids) {
        double pi=0.0,pk=0.0,x,y;
        int i;

        for(i=1;i<=1000000;i++) {
            x=Math.random()-0.5;
            y=Math.random()-0.5;
            if(i%10000==0)publishProgress(i/10000);
            if(Math.pow(x,2)+Math.pow(y,2)<=Math.pow(0.5,2))
                pk++;
        }
        pi=4*pk/1000000.0;

        return pi;
    }


    @Override
    protected void onPreExecute() {
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    protected void onPostExecute(Double aDouble) {

        n1EditText.setText(aDouble.toString());
        progressBar.setVisibility(ProgressBar.INVISIBLE);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        progressBar.setProgress(values[0]);
    }
}


    //OSOBNY watek-koniec

    //Kolejny WATEK
    Handler myHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String primes = msg.getData().getString("primes");
            primResult.setText(primes);

        }
    };


    public void primeButtonClick(View view) {


        new Thread(new Runnable() {
            @Override
            public void run() {
                Message m = myHandler.obtainMessage();
                Bundle b = new Bundle();
                String msg="";

                int pierw, pom;
                pierw = (int) Math.sqrt(result);
                int k=2, rr= (int) (Math.round(result));

                while(rr >1 && k<=pierw) {
                    while(rr %k==0) {
                        msg=msg+String.valueOf(k)+" ";

                        rr =rr/k;
                    }
                    k=k+1;
                }

                if(rr>1){
                    msg=msg+rr;

                }
                b.putString("primes",msg);
                m.setData(b);
                myHandler.sendMessage(m);

            }
        }).start();

    }
//Kolejny WATEK-koniec



//on create
progressBar=(ProgressBar)findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.INVISIBLE);



//obsuga

public void piButtonClick(View view){
        progressBar.setProgress(0);
        new PiComputeTask().execute();
        }


